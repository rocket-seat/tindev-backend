const express = require('express')
const mongoose = require('mongoose')

mongoose.connect('mongodb+srv://usuario:senha@banco-avdfa.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true })

const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)

const connectedUsers = {}

io.on('connection', socket => {
    const { user } = socket.handshake.query
    connectedUsers[user] = socket.id
    console.log('Client connectet:', user)
})

app.use((req, res, next) => {
    req.io = io;
    req.connectedUsers = connectedUsers;
    return next();
})

const cors = require('cors')
app.use(cors())

app.use(express.json())

const routes = require('./routes')
app.use(routes)

server.listen(3333, () => {
    console.log('Servidor rodando...')
})