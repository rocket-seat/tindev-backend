const axios = require('axios')

const Dev = require('../models/Dev')

module.exports = {
    async index(req, res) {
        const { user } = req.headers

        const loggedDev = await Dev.findById(user)

        const registros = await Dev.find({
            $and: [
                { _id: { $ne: user } },
                { _id: { $nin: loggedDev.likes } },
                { _id: { $nin: loggedDev.dislikes } }
            ]
        }).sort({ _id: -1 })

        return res.json(registros)
    },

    async getAll(req, res) {
        const registros = await Dev.find({})
        return res.json(registros)
    },

    async create(req, res) {
        const { username } = req.body

        const userExists = await Dev.findOne({ user: username })

        if (userExists) {
            return res.json(userExists)
        }

        const response = await axios.get('https://api.github.com/users/' + username)

        const { name, bio, avatar_url: avatar } = response.data

        const dev = await Dev.create({
            name,
            user: username,
            bio,
            avatar
        })

        return res.json(dev)
    }
}