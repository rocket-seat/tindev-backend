const axios = require('axios')

const Dev = require('../models/Dev')

module.exports = {
    async create(req, res) {
        const user = req.headers.user
        const devId = req.params.devId

        const loggedDev = await Dev.findById(user)
        const targetDev = await Dev.findById(devId)

        if (!targetDev) {
            return res.status(400).json({ error: 'O registro informado não existe.' })
        }

        loggedDev.dislikes.push(targetDev._id)
        await loggedDev.save()

        return res.json(loggedDev)
    }
}