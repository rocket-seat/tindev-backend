const axios = require('axios')

const Dev = require('../models/Dev')

module.exports = {
    async create(req, res) {
        const user = req.headers.user
        const devId = req.params.devId

        const loggedDev = await Dev.findById(user)
        const targetDev = await Dev.findById(devId)

        if (!targetDev) {
            return res.status(400).json({ error: 'O registro informado não existe.' })
        }

        loggedDev.likes.push(targetDev._id)
        await loggedDev.save()

        if (targetDev.likes.includes(loggedDev._id)) {
            const loggedSocket = req.connectedUsers[user]
            const targetSocket = req.connectedUsers[devId]

            if (loggedSocket) {
                req.io.to(loggedSocket).emit('match', targetDev)
            }

            if (targetSocket) {
                req.io.to(targetSocket).emit('match', loggedDev)
            }
        }

        return res.json(loggedDev)
    }
}